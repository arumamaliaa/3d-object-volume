// // Import readline
// const readline = require("readline");
// const index.rl = readline.createInterface({
//   inputCone: process.stdin,
//   output: process.stdout,
// });

// function isEmptyOrSpaces(str) {
//   return str === null || str.match(/^ *$/) !== null;
// }

const index = require("../index"); // Import index to run index.rl on this file

/* Cone Volume */
function calculateConeVolume(r, h) {
  let phi = 3.14;

  return (1 / 3) * phi * r ** 2 * h;
}

function inputCone() {
  index.rl.question("Radius: ", (r) => {
    index.rl.question("Height: ", (h) => {
      if (!isNaN(r) && !isNaN(h) && !index.isEmptyOrSpaces(r, h)) {
        console.log(`Cone volume is ${calculateConeVolume(r, h)} cm3 \n`);

        index.rl.close();
      } else {
        console.log("The inputCone must be number! \n");
        inputCone();
      }
    });
  });
}

// module.exports.index.rl = index.rl;
// module.exports.isEmptyOrSpaces = isEmptyOrSpaces;
module.exports = { inputCone }; // Export the input, so the another file can run this code
