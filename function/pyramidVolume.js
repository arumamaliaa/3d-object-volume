const index = require("../index"); // Import index to run rl on this file

function pyramidVolume(s, t) {
  const luasAlas = s ** 2;
  volumeLimas = (luasAlas * t) / 3;

  return volumeLimas;
}

function inputPyramidbased() {
  index.rl.question(`Pyramid base side length: `, (s) => {
    if (!isNaN(s) && !index.isEmptyOrSpaces(s)) {
      inputHeight(s);
    } else {
      console.log(`Pyramid base side length must be a number\n`);
      inputPyramidbased();
    }
  });
}

function inputHeight(s) {
  index.rl.question(`Pyramid Height: `, (t) => {
    if (!isNaN(t) && !index.isEmptyOrSpaces(t)) {
      console.log(`\nPyramid Volume: ${pyramidVolume(s, t)}`);
      index.rl.close();
    } else {
      console.log(`Height must be a number\n`);
      inputHeight(s, t);
    }
  });
}

module.exports = { inputPyramidbased }; // Export the input, so the another file can run this code
